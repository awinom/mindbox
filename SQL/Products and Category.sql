﻿
--region CREATE Создание таблиц

-- Таблица Продуктов 
IF OBJECT_ID('Product') IS NULL
CREATE TABLE Product
  (
    product_id   INT IDENTITY PRIMARY KEY CLUSTERED
  , product_name NVARCHAR(255) COLLATE Cyrillic_General_BIN NOT NULL  
  )

-- Таблица категорий
IF OBJECT_ID('Category') IS NULL
CREATE TABLE Category
  (
    category_id   INT IDENTITY PRIMARY KEY CLUSTERED
  , category_name NVARCHAR(255) COLLATE Cyrillic_General_BIN NOT NULL 
  )

-- Таблица категорий с продуктами
IF OBJECT_ID('Product_Сategories') IS NULL
CREATE TABLE Product_Сategories
  (
    ident        INT IDENTITY 
  , product_id   INT NOT NULL
  , category_id  INT   
  )
CREATE NONCLUSTERED INDEX IX_PRODUCT_CATEGORIES_product_id_category_id ON Product_Сategories (product_id, category_id);

ALTER TABLE Product_Сategories
ADD CONSTRAINT FK_PRODUCT_CATEGORIES_REFERENCE_PRODUCT FOREIGN KEY (product_id) REFERENCES Product (product_id) ON DELETE CASCADE
GO

ALTER TABLE Product_Сategories
ADD CONSTRAINT FK_PRODUCT_CATEGORIES_REFERENCE_CATEGORY FOREIGN KEY (category_id) REFERENCES Category (category_id) ON DELETE CASCADE
GO
--endregion

--region INSERT Заполнение таблиц
INSERT INTO Product (product_name)
VALUES ('Киндер сюрприз')       --1
      ,('Футбольный мяч')
      ,('Апельсин')
      ,('Капибара')
      ,('Спантч Боб')           --5
      ,('Футболка')
      ,('Кружка')
      ,('Баскетбольный мяч')
      ,('Яблочный сок')
      ,('Пицца')                --10
      ,('Чебурек')
      ,('Активированный Уголь') --12

INSERT INTO Category (category_name)
VALUES ('Игрушки')              --1
      ,('Сладости')             
      ,('Спортивный инвентарь')
      ,('Фрукты')
      ,('Еда')                  --5
      ,('Моющие средства')
      ,('Выпечка')
      ,('Напитки')
      ,('Автомобили')
      ,('Картины')              --10


INSERT INTO Product_Сategories (product_id,category_id)
VALUES (1,1)      --1
      ,(1,2)
      ,(1,3)
      ,(2,1)
      ,(2,3)      --5
      ,(3,4)
      ,(3,5)
      ,(4,1)
      ,(5,1)
      ,(5,6)      --10
      ,(6,3)
      ,(7,null)
      ,(8,1)
      ,(8,3)
      ,(9,8)      --15
      ,(10,5)
      ,(10,7)
      ,(11,5)
      ,(11,7)
      ,(12,null)  --20
--endregion

--region Select (SQL запрос)
--Выводим Все Продукты и все Категории 
SELECT p.product_name,c.category_name
  FROM Product p
  FULL JOIN Product_Сategories pc ON pc.product_id = p.product_id
  FULL JOIN Category c ON c.category_id = pc.category_id

--Выводим Все Продукты (не выводим Категории если у них нет продуктов)
SELECT p.product_name,c.category_name
  FROM Product p
  JOIN Product_Сategories pc ON pc.product_id = p.product_id
  LEFT JOIN Category c ON c.category_id = pc.category_id

--P.s. В тз не сказано, что делать если у категории нет ни одного продукта, поэтому сделал 2 запроса 
--endregion