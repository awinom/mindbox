﻿using System;
using System.Linq;
using ShapeCalculator.Interface;

namespace ShapeCalculator.Shape
{
    /// <summary>
    /// Треугольник
    /// </summary>
    public class Triangle : IShape
    {
        /// <summary>
        /// Массив из длин сторон треугольника
        /// </summary>
        private readonly double[] side;

        /// <summary>
        /// Полупериметр треугольника
        /// </summary>
        private double halfPerimeter;

        /// <summary>
        /// Конструктор принимает стороны треугольника
        /// </summary>
        /// <param name="side_1">Первая сторона треугольника</param>
        /// <param name="side_2">Вторая сторона треугольника</param>
        /// <param name="side_3">Третья сторона треугольника</param>
        public Triangle(double side_1, double side_2, double side_3)
        {
            if (side_1 <= 0 || side_2 <= 0 || side_3 <= 0)
                throw new ArgumentException("Сторона должна быть больше 0");

            if (side_1 + side_2 <= side_3 || side_1 + side_3 <= side_2 || side_2 + side_3 <= side_1)
                throw new ArgumentException("Сумма длинн двух сторон треугольника должна быть больше третьей стороны");

            side = new double[3] { side_1, side_2, side_3 };
              
        }

        /// <summary>
        /// Возвращает площадь треугольника по формуле Герона
        /// </summary>
        public double GetArea()
        {
            halfPerimeter = (side[0] + side[1] + side[2]) / 2;
            return Math.Sqrt(halfPerimeter * (halfPerimeter - side[0]) * (halfPerimeter - side[1]) * (halfPerimeter - side[2]));
        }

        /// <summary>
        /// Проверка на то, является ли треугольник прямоугольным
        /// Точность: 1E-8
        /// </summary>
        public bool IsRectangular()
        {
            // Допустимая погрешность на длинну стороны
            double eps = 1E-8;
            double[] arraySideSort = side.OrderByDescending(x => x).ToArray();
            return Math.Abs(Math.Pow(arraySideSort[0],2) - Math.Pow(arraySideSort[1], 2) - Math.Pow(arraySideSort[2], 2)) <= eps*6;
        }
    }
}
